let express = require('express');
let bodyParser = require('body-parser');
let rawBody = require('./node_modules/bitauth/lib/middleware/rawbody');
let bitauthMiddleware = require('./node_modules/bitauth/lib/middleware/bitauth');
let path = require('path');
let app = express();
// var pizzas = [];
let users = {
  'Tf7UNQnxB8SccfoyZScQmb34V2GdEtQkzDz': {name: 'Alice'}
};
let port = 3000;

// TODO: Activar CORS en express
// SINs https://en.bitcoin.it/wiki/Identity_protocol_v1 ¿endpoint?

app.use(rawBody);
app.use(bodyParser());

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/login', bitauthMiddleware, function(req, res) {
  if(!req.sin || !users[req.sin]) {console.log('entra'); return res.redirect('/login'); }
  // return res.send(401, {error: 'Unauthorized'});
  res.send(200, users[req.sin]);
});


/* app.post('/pizzas', bitauthMiddleware, function(req, res) {
  if(!req.sin || !users[req.sin]) return res.send(401, {error: 'Unauthorized'}); // redirect login
  var pizza = req.body;
  pizza.owner = users[req.sin].name;
  pizzas.push(pizza);
  res.send(200, req.body);
});

app.get('/pizzas', function(req, res) {
  res.send(200, pizzas);
}); */

app.listen(port, function() {
  console.log('Listening on http://localhost:' + port)
});

